"""Tests for hiddenv.conf.settings and hiddenv.config.Config."""

import importlib
import os
import unittest
from typing import NamedTuple
from uuid import UUID

import django.conf

from hiddenv.conf import ENVIRONMENT_VARIABLE, settings
from hiddenv.environment import Env


# pylint: disable = unused-argument
def hello(
    question: str = "qwerty",  # noqa: F841
    hah: str = "qwerty",  # noqa: F841
    heh: str = "qwerty",  # noqa: F841
    _ha: str = "qwerty",  # noqa: F841
):
    """docstring.

    Args:
        question: haha
        hah:
        heh:
        _ha:
    """


class Hello(NamedTuple):  # pylint: disable = missing-class-docstring
    question: str
    Question: str


class TestSettings(unittest.TestCase):
    """Tests for hiddenv.conf.settings and hiddenv.config.Config."""

    vars = (
        "SOMETHING",
        "AB_QWE",
        "ABC_HEL",
        "SMX",
        "SOMEY",
        "AB_AAA",
        "AB_BBB",
        "EXTRA",
        "HELLO",
        "TUPLE",
        "DICT",
        "BYE",
        "OVERRIDE",
        ENVIRONMENT_VARIABLE,
    )

    def setUp(self):
        """Resets settings and ensures environment doesn't have unexpected variables."""

        super().setUp()
        for obj in self.vars:
            assert obj not in os.environ, (obj, os.environ[obj])
        assert os.environ.get(django.conf.ENVIRONMENT_VARIABLE, "hiddenv.conf.settings") == "hiddenv.conf.settings"
        os.environ["AB_QWE"] = "3"
        os.environ["ABC_HEL"] = "00000000-0000-0000-0000-000000000000"
        os.environ["HELLO"] = "{\"one\": null}"
        os.environ["BYE"] = "[true]"
        os.environ["EXTRA"] = "1,2"
        os.environ["TUPLE"] = "1,one"
        os.environ["DICT"] = "1=true;2="
        os.environ["OVERRIDE"] = "2"
        settings._reset()  # pylint: disable = protected-access
        django.conf.settings._wrapped = django.conf.empty  # pylint: disable = protected-access
        assert not django.conf.settings.configured

    def tearDown(self):
        """Removes extraneous variables from environment."""

        super().tearDown()
        for obj in (*self.vars, django.conf.ENVIRONMENT_VARIABLE):
            os.environ.pop(obj, None)

    def test_config_extend(self):
        """Tests extending Config class."""

        with self.assertWarns(UserWarning) as cm:
            cls = getattr(importlib.import_module("hiddenv.tests.settingstest"), "SomeConfig")
        self.assertEqual(str(cm.warning), "hiddenv.tests.settingstest.Hello missing attribute HELL from __doc__")

        with self.assertWarns(UserWarning):
            cls_extended = cls.extend_for(target=hello, exclude=("NULLABLE", "hah", "HEH"))

        with self.assertWarns(UserWarning):
            self.assertIs(cls.extend_for(target=hello)(namespace="AB").NULLABLE, None)
        with self.assertRaises(AttributeError):
            assert cls_extended(namespace="AB").NULLABLE

        self.assertEqual(cls_extended(namespace="AB").EMPTY_TUPLE, ())
        self.assertEqual(cls_extended(namespace="AB").QUESTION, "qwerty")
        with self.assertRaises(AttributeError):
            assert cls_extended(namespace="AB").HAH
        with self.assertRaises(AttributeError):
            assert cls_extended(namespace="AB").HEH

        # test property super
        self.assertEqual(cls_extended(namespace="AB").TUPLE, (2, "asa"))
        with self.assertWarns(UserWarning):
            cls_base_extended = cls.__base__.extend_for(target=hello, exclude=("hah", "HEH"))
            self.assertEqual(cls_base_extended(namespace="AB").TUPLE, (2, "as"))

    def test_config_extend_descriptions(self):
        """Tests description results when extending Config class."""

        cls = getattr(importlib.import_module("hiddenv.tests.settingstest"), "SomeConfig")

        with self.assertWarns(UserWarning):
            cls_extended = cls.extend_for(target=hello, exclude=("NULLABLE", "hah", "HEH"))

        self.assertEqual(cls_extended.__doc__.partition("\n")[0], "docstring.")
        with self.assertWarns(UserWarning):
            descriptions = {None: "1"}  # yapf: disable
            self.assertEqual(cls.extend_for(hello, descriptions=descriptions).__doc__.partition("\n")[0], "1")

        self.assertEqual(cls_extended.__info__["QUESTION"].description, "haha")
        with self.assertWarns(UserWarning):
            self.assertEqual(cls.extend_for(hello, descriptions={}).__info__["QUESTION"].description, "")
        with self.assertWarns(UserWarning):
            self.assertEqual(
                cls.extend_for(hello, descriptions=dict(QUESTION="questioners")).__info__["QUESTION"].description,
                "questioners",
            )

    def test_config_extend_errors(self):
        """Tests errors and warnings when extending Config class."""

        cls = getattr(importlib.import_module("hiddenv.tests.settingstest"), "SomeConfig")

        with self.assertRaises(TypeError) as cm:
            cls.extend_for(target=hello, raise_error=True)
        self.assertEqual(
            str(cm.exception),
            f"while extending hiddenv.tests.settingstest.SomeConfig for {hello}, "
            f"skipping parameter _ha: the generated setting name _HA is invalid",
        )

        with self.assertRaises(TypeError) as cm:
            cls.extend_for(target=lambda *args: None, raise_error=True)
        self.assertEqual(
            str(cm.exception).partition(" <")[0],
            "while extending hiddenv.tests.settingstest.SomeConfig for",
        )
        self.assertEqual(
            str(cm.exception).partition(">, ")[2],
            "skipping parameter args: variadic parameters not allowed",
        )
        with self.assertWarns(UserWarning):
            cls.extend_for(target=lambda *args: None)

        with self.assertRaises(TypeError) as cm:
            cls.extend_for(target=lambda hyh: None, raise_error=True)
        self.assertEqual(
            str(cm.exception).partition(" <")[0],
            "while extending hiddenv.tests.settingstest.SomeConfig for",
        )
        self.assertEqual(
            str(cm.exception).partition(">, ")[2],
            "skipping parameter hyh: parameter missing annotation",
        )
        with self.assertWarns(UserWarning):
            cls.extend_for(target=lambda hyh: None)

        with self.assertRaises(TypeError) as cm:
            with self.assertWarns(UserWarning):
                cls.extend_for(target=Hello)
        self.assertEqual(
            str(cm.exception),
            f"while extending hiddenv.tests.settingstest.SomeConfig for {Hello!r}, "
            f"skipping parameter Question: duplicate setting name QUESTION",
        )

        with self.assertWarns(UserWarning):
            cls_extended = cls.extend_for(target=hello, exclude=("NULLABLE", "hah", "HEH"))
        hello.__annotations__["question"] = int
        with self.assertRaises(TypeError) as cm:
            with self.assertWarns(UserWarning):
                cls_extended.extend_for(target=hello)
        self.assertEqual(
            str(cm.exception),
            f"while extending hiddenv.config.SomeConfig for {hello!r}, "
            f"skipping parameter question: the annotation int does not match old annotation str",
        )
        hello.__annotations__["question"] = str

    def test_config_instance(self):
        """Tests namespace validation, Env retrieval and attribute setting."""

        cls = getattr(importlib.import_module("hiddenv.tests.settingstest"), "SomeConfig")

        assert isinstance(cls(namespace="A").env, Env)
        with self.assertRaises(ValueError) as cm:
            cls(namespace="_")
        self.assertEqual(str(cm.exception), "Invalid namespace '_'")
        with self.assertRaises(ValueError) as cm:
            cls.extend(namespace="_")
        self.assertEqual(str(cm.exception), "Invalid namespace '_'")
        with self.assertRaises(AssertionError):
            cls(namespace="A", _asd=str)

        with self.assertWarns(UserWarning):
            self.assertEqual(cls(namespace="AB", qwe=str).WHAT, "4 -- 4")
        with self.assertWarns(UserWarning):
            self.assertEqual(cls(namespace="AB", qwe=str).SS.SOMEY, "")
        with self.assertWarns(UserWarning):
            self.assertEqual(cls(namespace="AB", qwe=str).load(lambda what: f"|{what}|"), "|4 -- 4|")

    def test_dotenv_generation(self):
        """Tests generation of dotenv strings."""

        cls = getattr(importlib.import_module("hiddenv.tests.settingstest"), "SomeConfig")
        self.assertEqual(
            cls.dotenv,
            "### hiddenv.tests.settingstest.SomeConfig2"
            "\n"
            "\n# setting SOMEY (default '')"
            "\n#SOMEY="
            "\n"
            "\n# setting OVERRIDE (default 1)"
            "\n#OVERRIDE="
            "\n"
            "\n"
            "\n### Settings related to second option."
            "\n### Longer description."
            "\n"
            "\n# setting SMX (default 2): smx"
            "\n#SMX="
            "\n"
            "\n# setting THING (default {})"
            "\n#THING="
            "\n"
            "\n# setting NULLABLE (default None)"
            "\n#NULLABLE="
            "\n"
            "\n# setting SS_SOMEY (default '')"
            "\n#SS_SOMEY=",
        )
        self.assertEqual(str(cls.__info__["SOMEX"]), "SOMEX: typing_extensions.Annotated[int, 'SMX'] = 2  # smx")
        self.assertEqual(
            getattr(importlib.import_module("hiddenv.tests.settingstest"), "AConfig").dotenv,
            "### Settings related to AB."
            "\n"
            "\n# setting AB_AAA (default 'A'): a"
            "\n#AB_AAA="
            "\n"
            "\n# setting AB_QWE: hello"
            "\nAB_QWE="
            "\n"
            "\n# setting AB_BBB (default 'B'): b"
            "\n#AB_BBB=",
        )
        with self.assertRaises(LookupError) as cm:
            assert settings.dotenv
        self.assertEqual(str(cm.exception), "Environment variable HIDDENV_SETTINGS_MODULE not set.")
        os.environ[django.conf.ENVIRONMENT_VARIABLE] = "hiddenv.tests.settingstest"
        self.assertEqual(
            settings.dotenv,
            "### hiddenv.tests.settingstest.SomeConfig2"
            "\n"
            "\n# setting SOMEY (default '')"
            "\n#SOMEY="
            "\n"
            "\n"
            "\n### Settings related to second option."
            "\n### Longer description."
            "\n"
            "\n# setting SMX (default 2): smx"
            "\n#SMX="
            "\n"
            "\n# setting THING (default {})"
            "\n#THING="
            "\n"
            "\n# setting NULLABLE (default None)"
            "\n#NULLABLE="
            "\n"
            "\n# setting SS_SOMEY (default '')"
            "\n#SS_SOMEY="
            "\n"
            "\n"
            "\n### hiddenv.tests.settingstest.Hello"
            "\n"
            "\n# setting ABC_HEL"
            "\nABC_HEL="
            "\n"
            "\n"
            "\n### Settings related to AB."
            "\n"
            "\n# setting AB_AAA (default 'A'): a"
            "\n#AB_AAA="
            "\n"
            "\n# setting AB_QWE: hello"
            "\nAB_QWE="
            "\n"
            "\n# setting AB_BBB (default 'B'): b"
            "\n#AB_BBB="
            "\n"
            "\n"
            "\n### Settings test module."
            "\n### Extra description."
            "\n"
            "\n# setting SOMETHING (default None): something is this"
            "\n#SOMETHING="
            "\n"
            "\n# setting SOMETHING_OTHER (default '')"
            "\n#SOMETHING_OTHER="
            "\n"
            "\n"
            "\n### Settings related to ABB."
            "\n"
            "\n# setting ABB_BBB_BBB (default 'bee bee bee')"
            "\n#ABB_BBB_BBB="
            "\n"
            "\n"
            "\n### Settings test module."
            "\n### Extra description."
            "\n"
            "\n# setting OTHER_SOMETHING (default '')"
            "\n#OTHER_SOMETHING="
            "\n"
            "\n# setting OVERRIDE"
            "\nOVERRIDE=",
        )

    def test_is_settings(self):
        """Tests setting name validity."""

        for obj in ("A", "AB", "ABC"):
            self.assertTrue(settings.is_setting(obj), obj)
            self.assertFalse(settings.is_setting(f"{obj}_"), obj)
            self.assertTrue(settings.is_setting(f"{obj}_{obj}"), obj)
            self.assertTrue(settings.is_setting(f"{obj}__{obj}"), obj)
            self.assertFalse(settings.is_setting(f"{obj}___{obj}"), obj)
            self.assertTrue(settings.is_setting(f"{obj}__9"), obj)
            self.assertTrue(settings.is_setting(f"{obj}__9{obj}"), obj)
            self.assertTrue(settings.is_setting(f"{obj}__{obj}9"), obj)
            self.assertTrue(settings.is_setting(f"{obj}__{obj}9_{obj}9"), obj)
            self.assertTrue(settings.is_setting(f"{obj}__{obj}9__00"), obj)
            self.assertFalse(settings.is_setting(f"{obj}__{obj}9_{obj}9".lower()), obj)

    def test_settings_annotations_include(self):
        """Tests `hiddenv.conf.settings` setup.

        Utilizes a module with `Config` classes in its `.__annotations__` and `__include__`.

        Also test settings match to `django.conf.settings`.
        """

        os.environ["SOMETHING"] = "one=3;two=4"
        os.environ["AB_QWE"] = "3"
        os.environ["ABC_HEL"] = "9e774aa1-e307-46eb-a4e6-7274cd323b05"
        os.environ["SMX"] = "3"
        os.environ["SOMEY"] = "why"
        os.environ["AB_AAA"] = "AA"
        os.environ["AB_BBB"] = "BB"
        os.environ[django.conf.ENVIRONMENT_VARIABLE] = "hiddenv.tests.settingstest"

        self.assertEqual(django.conf.settings.CONFIGURATION, "jejee")
        with self.assertWarns(UserWarning) as cm:
            self.assertEqual(settings.CONFIGURATION, "jejee")
        self.assertEqual(str(cm.warning), "django settings configured before hiddenv; resetting django settings")

        settings.env = settings.env
        with self.assertRaises(AttributeError) as cm:
            settings.name_pattern = ""
        self.assertEqual(str(cm.exception), "Can't set class attribute 'name_pattern' on Settings object")

        with self.assertRaises(ValueError) as cm:
            settings._setup()  # pylint: disable = protected-access
        self.assertEqual(str(cm.exception), "Already configured.")

        settings._reset()  # pylint: disable = protected-access
        self.assertEqual(settings.CONFIGURATION, "jejee")
        self.assertEqual(django.conf.settings.CONFIGURATION, "jejee")
        self.assertEqual(django.conf.settings.COMMON, "THAT")
        self.assertEqual(settings.COMMON, "THAT")
        self.assertEqual(django.conf.settings.SOMEX, 6)
        self.assertEqual(settings.SOMEX, 6)
        self.assertEqual(settings.WHAT, "6 -- 6")
        self.assertEqual(django.conf.settings.SOMEY, "why")
        self.assertEqual(settings.SOMEY, "why")
        self.assertEqual(settings.AB.AAA, "AA (3)")
        self.assertEqual(settings.AB.BBB, "BB (3)")
        self.assertEqual(settings.LIST, [1, 2])
        self.assertEqual(settings.HELL, UUID("9e774aa1-e307-46eb-a4e6-7274cd323b05"))
        self.assertEqual(settings.OVERRIDE, "2")
        with self.assertRaises(AttributeError) as cm:
            assert settings._AAAA  # pylint: disable = protected-access
        self.assertEqual(str(cm.exception), "'_AAAA' is not a valid setting name")
        with self.assertRaises(AttributeError) as cm:
            assert settings.BBB
        self.assertEqual(str(cm.exception), "module 'hiddenv.conf.settings' has no attribute 'BBB'")
        self.assertEqual(
            settings.SOMETHING,
            {
                "one": 3,
                "two": 4,
            },
        )
        self.assertEqual(settings.AB.QWE, 3)
        self.assertIs(settings.app, None)
        self.assertIs(settings.AB.app, None)
        # test property super
        self.assertEqual(settings.TUPLE, (2, "asa"))

    def test_settings_simple(self):
        """Tests `hiddenv.conf.settings` setup.

        Utilizes a module with no `.__annotations__` or `__include__`.

        Also test settings match to `django.conf.settings`.
        """

        os.environ[ENVIRONMENT_VARIABLE] = "hiddenv.tests.settingstest_1"
        self.assertEqual(django.conf.settings.BBB, 123)
        self.assertEqual(settings.COMMON, "THIS")
        self.assertEqual(settings.HELLO, dict(one=None))
        self.assertEqual(settings.BYE, [True])
        self.assertEqual(settings.EXTRA, [1, 2])
        self.assertEqual(settings.TUPLE, (1, "one"))
        self.assertEqual(
            settings.DICT,
            {
                1: True,
                2: False,
            },
        )
        with self.assertRaises(AttributeError):
            assert settings.CONFIGURATION
        self.assertEqual(settings.app, "hiddenv.tests")

    def test_tuple_fail(self):
        """Tests reading tuple annotation and getting something other than a expected tuple."""

        os.environ[ENVIRONMENT_VARIABLE] = "hiddenv.tests.settingstest_1"
        os.environ["TUPLE"] = "1"
        with self.assertRaises(ValueError) as cm:
            assert settings.TUPLE
        self.assertEqual(str(cm.exception), "Expected 2-tuple, got ('1',)")

    def test_json_list_fail(self):
        """Tests reading JSONList annotation and getting something other than a list."""

        os.environ[ENVIRONMENT_VARIABLE] = "hiddenv.tests.settingstest_1"
        os.environ["HELLO"] = '{"one": null}'
        os.environ["BYE"] = '{"one": null}'
        with self.assertRaises(ValueError) as cm:
            assert settings.BBB
        self.assertEqual(str(cm.exception), "environment variable BYE did not produce a list, got {'one': None}")

    def test_json_dict_fail(self):
        """Tests reading JSONDict annotation and getting something other than a dict."""

        os.environ[ENVIRONMENT_VARIABLE] = "hiddenv.tests.settingstest_1"
        os.environ["HELLO"] = "[]"
        os.environ["BYE"] = "[]"
        with self.assertRaises(ValueError) as cm:
            assert settings.BBB
        self.assertEqual(str(cm.exception), "environment variable HELLO did not produce a dict, got []")

    def test_no_settings_module(self):
        """Tests for expected exceptions in `hiddenv.conf.settings` setup when source module cannot be found."""

        with self.assertRaises(LookupError) as cm:
            assert settings.ASD
        self.assertEqual(str(cm.exception).strip("'"), "Environment variable HIDDENV_SETTINGS_MODULE not set.")

        os.environ[ENVIRONMENT_VARIABLE] = "hiddenv.tests.settingstest_not_there"
        with self.assertRaises(ModuleNotFoundError) as cm:
            assert settings.ASD
        self.assertEqual(str(cm.exception), "No module named 'hiddenv.tests.settingstest_not_there'")

    def test_load(self):
        """Tests loading a class from settings."""

        class Test(NamedTuple):
            """Test class."""

            truthy: bool
            common: str

        os.environ[ENVIRONMENT_VARIABLE] = "hiddenv.tests.settingstest"
        self.assertEqual(
            settings.load(Test),  # type: ignore
            Test(truthy=True, common="THAT"),
        )

"""Tests for hiddenv.dotenv."""

import os

from hiddenv import dotenv
from hiddenv.tests import BaseTestCase, temporary_dotenv_file


class DotEnvTest(BaseTestCase):
    """Tests for hiddenv.dotenv."""

    def _test(self, data, expected):
        self.assertEqual(set(), {*data}.difference({*expected}))
        self.assertEqual(set(), {*expected}.difference({*data}))
        for key, value in expected.items():
            self.assertEqual(value, data[key])

    def test_values(self):
        """Tests parsing using content string."""

        data = dotenv.parse(self.test_file_content)
        self._test(data, self.expected)

    def test_failures(self):
        """Tests failure exceptions for dotenv parsing."""

        with self.assertRaises(ValueError) as cm:
            dotenv.parse(self.test_file_content + "\nONE_TWO=\"foo\\${BAR}${BAR}\"")
        self.assertEqual(str(cm.exception), "Unable to process line  foo\\${BAR}${BAR}")

        with self.assertRaises(ValueError) as cm:
            dotenv.parse(self.test_file_content + "\nONE_TWO=\"foo\\$BAR$BAR\"")
        self.assertEqual(str(cm.exception), "Unable to process line  foo\\$BAR$BAR")

    def test_file(self):
        """Tests parsing using temporary file."""

        file = temporary_dotenv_file(self.test_file_content)
        data = dotenv.read_dotenv(file.name)
        self._test(data, self.expected)

    def test_real_file(self):
        """Tests parsing using file."""

        with open(self.test_file_name, "w") as f:
            f.write(self.test_file_content)
        file_path = dotenv.find_dotenv(filename=self.test_file_name)
        self.assertEqual(dotenv.find_dotenv(find=False, filename=self.test_file_name), file_path)
        os.environ.update(DOTENV_PATH=file_path)
        self.assertEqual(dotenv.find_dotenv(), file_path)
        data = dotenv.read_dotenv(dotenv.find_dotenv(filename=self.test_file_name))
        self._test(data, self.expected)
        os.remove(self.test_file_name)

    def test_file_not_found(self):
        """Tests for expected failures."""

        self.assertEqual(dotenv.find_dotenv(filename="nothing.env"), None)
        self.assertEqual(dotenv.find_dotenv(filename="nothing.env", find=False), None)
        self.assertEqual(dotenv.read_dotenv(None), None)
        self.assertEqual(dotenv.read_dotenv("nothing.env"), None)

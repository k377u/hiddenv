ENV_NAME?=env
PYTHON?=python3
PACKAGE_NAME:="$(subst _,-,$(shell grep 'name = .*' setup.cfg | cut -d ' ' -f3-))"
PACKAGE_DIR="$(subst -,_,$(PACKAGE_NAME))"

MIN_PYLINT_SCORE=100
MIN_COVERAGE=99

env-test:
	@if ! test -d $(ENV_NAME) ; \
	then echo "No virtualenv '$(ENV_NAME)' found"; \
	exit 1; \
	fi

env-build:
	@echo
	@test -d $(ENV_NAME) || virtualenv --python=$(PYTHON) $(ENV_NAME)
	@echo

env-upgrade: env-test
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		pip install pip setuptools wheel twine --upgrade ; \
	)
	@echo

install:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		pip install -e .[dev]; \
	)
	@echo

env: env-build env-upgrade

dev: env install
	@echo
	@echo "Activate virtualenv with:"
	@echo
	@echo ". $(ENV_NAME)/bin/activate"
	@echo

isort:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		isort $(PACKAGE_DIR)/ -m 3 --check-only; \
	)
	@echo

isort-modify:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		isort $(PACKAGE_DIR)/ -m 3; \
	)
	@echo

pep8:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		pycodestyle $(PACKAGE_NAME); \
	)
	@echo

yapf:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		find ./$(PACKAGE_DIR) -name '*.py' -print0 | xargs -0 yapf --style setup.cfg --diff ; \
		find ./$(PACKAGE_DIR) -name '*.pyi' -print0 | xargs -0 yapf --style setup.cfg --diff ; \
	)
	@echo

yapf-modify:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		find ./$(PACKAGE_DIR) -name '*.py' -print0 | xargs -0 yapf --style setup.cfg --in-place ; \
		find ./$(PACKAGE_DIR) -name '*.pyi' -print0 | xargs -0 yapf --style setup.cfg --in-place ; \
	)
	@echo

pylint:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		pylint --rcfile=setup.cfg $(PACKAGE_NAME); \
	)
	@echo

pylint-html:
	@echo
	@mkdir -p test_result
	@( \
		. $(ENV_NAME)/bin/activate; \
		pylint --rcfile=setup.cfg $(PACKAGE_NAME) --load-plugins pylint_json2html --output-format jsonextended | \
		pylint-json2html -f jsonextended > test_result/pylint.html; \
		python -c 'f=open("test_result/pylint.html"); score = float(f.read().partition("<h2>Score</h2>")[2].partition("/")[0].strip()) * 10;print(f"Pylint score: {score}%\n\n");assert score >= $(MIN_PYLINT_SCORE), f"\n\n    Pylint score {score}% < {float($(MIN_PYLINT_SCORE))}%\n\n";'; \
	)
	@echo

mypy:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		mypy -p $(PACKAGE_DIR); \
	)
	@echo

test:
	@echo
	@mkdir -p test_result
	@( \
		. $(ENV_NAME)/bin/activate; \
		pytest $(PACKAGE_DIR)/tests/ \
			--cov=$(PACKAGE_NAME) \
			--html=test_result/report.html \
			--cov-report html:test_result/ ; \
		python -c 'f=open("test_result/index.html"); score = float(f.read().partition("<tr class=\"total\">")[2].partition("<td class=\"right\"")[2].partition(">")[2].partition("%")[0].strip());print(f"Coverage score: {score}%\n\n");assert score >= $(MIN_COVERAGE), f"\n\n    Coverage {score}% < {float($(MIN_COVERAGE))}%\n\n";'; \
	)
	@echo

clean:
	@echo
	-rm -fr .eggs/ .cache/ *.egg-info *.egg $(ENV_NAME)
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -type d -name "__pycache__" -delete
	-rm -fr .pytest_cache/ .mypy_cache/ test_result/ .coverage
	-rm -fr build/ dist/ *.tar.gz
	@echo

# ToDo: set development status classifier
version-bump:
	@( \
		. $(ENV_NAME)/bin/activate; \
		python -c 'from datetime import date; f=open("hiddenv/__init__.py"); start ,_ , end = f.read().partition("\n__version__ = \""); f.close(); version, _, end = end.partition("\"\n"); td = date.today(); y,m,v = map(int, version.split(".")); ver = f"{y}.{m}.{v+1}" if td.year == y and td.month == m else f"{td.year}.{td.month}.1"; f=open("hiddenv/__init__.py", "w"); f.write(f"{start}\n__version__ = \"{ver}\"\n{end}");' ;\
		python -c 'from datetime import date; f=open("LICENSE"); start ,_ , end = f.read().partition("\nCopyright (c) "); f.close(); year, _, end = end.partition(" "); td = date.today(); f=open("LICENSE", "w"); f.write(f"{start}\nCopyright (c) {date.today().year} {end}");' ;\
	)


build-dist:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		python setup.py sdist bdist_wheel; \
	)
	@echo


twine-check:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		twine check dist/* ; \
	)
	@echo


twine-upload:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		twine upload --config-file $(HOME)/.pypirc dist/* ; \
	)
	@echo
